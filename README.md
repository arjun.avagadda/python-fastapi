# python-fastapi

## create virtual env and install packages

```python
pip install fastapi

pip install "uvicorn[standard]"
```

## Running the program

> uvicorn main:app --reload

```
>  main: the file main.py (the Python "module").
>  app: the object created inside of main.py with the line app = FastAPI().
> --reload: make the server restart after code changes. Only use for development.
```

## ROUTES (path operatrion)

http://127.0.0.1:8000/docs -> automatic interactive API documentation

/redoc -> alternative automatic documentation


## schema

pydantic library - to define schema

## CRUD

![fastAPICRUD](assets/fastapi_crud.png "fastAPI-CRUD")