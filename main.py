from typing import Optional
from fastapi import Body, FastAPI
from pydantic import BaseModel

app = FastAPI()

class POST(BaseModel):
    title: str
    content: str
    published: bool = True
    rating: Optional[int] = None


@app.get("/")
def root():
    return {"message": "Hell World"}


@app.get("/posts")
def get_posts():
    return {"message": "getting posts.."}


@app.post("/posts")
def create_post(new_post : POST):
    return {"new_post": new_post}
